Java/JavaFX project highlighting asynchronous events, templates, classes, 
inheritance, multi-threaded code, MVC, data sharing between controllers/models, 
2D user interaction, event listeners, lambdas, and other intermediate-advanced 
concepts.