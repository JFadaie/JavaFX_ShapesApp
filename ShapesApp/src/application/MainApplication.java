package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import controller.MainFrameController;
import model.ShapeManager;



public class MainApplication extends Application {
	private Stage 		m_primaryStage;
	private BorderPane  m_rootLayout;
	MainFrameController m_mainController;
	ShapeManager		m_ShapeManager;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			m_ShapeManager = ShapeManager.getShapeManager();
			assert( m_ShapeManager != null );
			System.out.println( "ShapeManager created!");
			
			m_primaryStage = primaryStage;
			m_primaryStage.setTitle("Shapes");
			
			initRootLayout();
			m_mainController.init();
			m_primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation( MainApplication.class.getResource("../view/MainFrame.fxml"));
			m_rootLayout = (BorderPane) loader.load();
			m_mainController = loader.getController();
			
			m_mainController.setDialogStage(m_primaryStage);
			m_mainController.setRootLayout(m_rootLayout);
			
			Scene scene = new Scene(m_rootLayout);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			m_primaryStage.setScene(scene);
	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	public static void main(String[] args) {
		launch(args);
	}
}
