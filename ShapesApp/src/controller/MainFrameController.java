package controller;



import java.io.IOException;

import application.MainApplication;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.IShape;
import controller.ShapeCanvasController;
import controller.ShapeOverviewController;
import controller.ToolsPanelController;

public class MainFrameController {
	private Stage 		m_dialogStage;
	private BorderPane  m_rootLayout;
	private ShapeCanvasController m_canvasController;
	private ShapeOverviewController m_overviewController;
	
	@FXML
	TabPane shapesTabPane;
	
	@FXML
	Tab		shapeCanvasTab, shapeOverviewTab;
	
	@FXML
	MenuBar menuBar;
	
	
	public void setDialogStage( Stage dialogStage ) {
		m_dialogStage = dialogStage;
	}
	
	public void setRootLayout( BorderPane rootLayout ) {
		m_rootLayout = rootLayout;
	}
	
	public void init() {
		showTabPanel();
		showToolPanel();
	}
	
	private void showTabPanel() {
		try {
			// Set Shape Canvas Tab.
			FXMLLoader canvasLoader = new FXMLLoader();
			canvasLoader.setLocation( MainApplication.class.getResource("../view/ShapeCanvas.fxml"));
			Group canvasLayout = (Group) canvasLoader.load();
			ShapeCanvasController canvasController = canvasLoader.getController();
			m_canvasController = canvasController;
			canvasController.setDialogStage(m_dialogStage);
			shapeCanvasTab.setClosable(false);
			shapeCanvasTab.setContent( canvasLayout );
			
			// Set Shape Overview Tab.
			FXMLLoader overviewLoader = new FXMLLoader();
			overviewLoader.setLocation( MainApplication.class.getResource("../view/ShapeOverview.fxml"));
			AnchorPane overviewLayout = (AnchorPane) overviewLoader.load();
			ShapeOverviewController overviewController = overviewLoader.getController();
			m_overviewController = overviewController;
			overviewController.setDialogStage(m_dialogStage);
			shapeOverviewTab.setClosable(false);
			shapeOverviewTab.setContent( overviewLayout );
		}
		catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
	private void showToolPanel() {
		try {
			FXMLLoader toolLoader = new FXMLLoader();
			toolLoader.setLocation( MainApplication.class.getResource("../view/ToolsPanel.fxml"));
			AnchorPane toolLayout = (AnchorPane) toolLoader.load();
			ToolsPanelController toolsController = toolLoader.getController();
			toolsController.setDialogStage(m_dialogStage);
			toolsController.setMainController( this );
			
			m_rootLayout.setLeft(toolLayout);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clearCanvas() {
		m_canvasController.clearCanvas();
	}
	
	public void redrawCanvas() {
		m_canvasController.redraw();
	}
	
	public void refreshOverviewTable() {
		m_overviewController.refreshTable();
	}
	
	public ReadOnlyBooleanProperty shapeOverviewTabVisible() {
		return shapeOverviewTab.selectedProperty();
	}
	
	public ReadOnlyIntegerProperty selectedIndexProperty() {
		return m_overviewController.getShapeTable().getSelectionModel().selectedIndexProperty();
	}
	
	public TableView<IShape> getShapeOverviewTable() {
		return m_overviewController.getShapeTable();
	}
	
}
