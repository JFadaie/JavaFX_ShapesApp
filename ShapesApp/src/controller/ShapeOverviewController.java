package controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import model.IShape;
import model.ShapeManager;
import model.ShapeTypes;

public class ShapeOverviewController {
	Stage 						m_dialogStage;
	
	@FXML
	TableColumn<IShape, String> nameColumn;
	
	@FXML
	TableColumn<IShape, String> shapeColumn;
	
	@FXML
	TableColumn<IShape, String> sizeColumn;
	
	@FXML
	TableColumn<IShape, String> colorColumn;
	
	@FXML
	TableColumn<IShape, String> xPosColumn;
	
	@FXML
	TableColumn<IShape, String> yPosColumn;
	
	@FXML
	TableView<IShape>  			shapeTable;
	
	/*
	 * Constructor
	 * The constructor is called before the initialize() method.
	 */
	public ShapeOverviewController() {
		
	}
	
	/*
	 * Initialize method called after the constructor and @FXML fields
	 * are populated.
	 */
	@FXML
	protected void initialize() {
        // Initialize the person table with the two columns.
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		shapeColumn.setCellValueFactory(cellData -> cellData.getValue().getTypeProperty());
		sizeColumn.setCellValueFactory(cellData -> cellData.getValue().getSizeProperty());
		colorColumn.setCellValueFactory(cellData -> cellData.getValue().getColorProperty());
		xPosColumn.setCellValueFactory(cellData -> cellData.getValue().getXPosProperty());
		yPosColumn.setCellValueFactory(cellData -> cellData.getValue().getYPosProperty());
	
		ShapeManager shapeManager = ShapeManager.getShapeManager();
		ObservableList<IShape> shapeList = shapeManager.getShapes();
		shapeTable.setItems( shapeList );
/*		ShapeManager shapeManager = ShapeManager.getShapeManager();
		ObservableList<IShape> shapeList = shapeManager.getShapes();
		shapeList.addListener( new ListChangeListener<IShape>() {
			
			@Override
			public void onChanged( ListChangeListener.Change<? extends IShape> change ) {
	          System.out.println("Detected a change to Observable List from Shape Overview! ");
	         
			}
		});*/
	}
	
	public void setDialogStage( Stage dialogStage ) {
		m_dialogStage = dialogStage;
	}
	
	public void refreshTable() {
		shapeTable.refresh();
	}
	
	public TableView<IShape> getShapeTable() {
		return shapeTable;
	}
}
