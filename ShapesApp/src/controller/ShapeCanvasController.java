package controller;

import java.awt.Event;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import model.IShape;
import model.ShapeManager;
import model.ShapeTypes;

public class ShapeCanvasController {
	private Stage m_dialogStage;

	private static final Integer LINE_WIDTH = 5;
	private IShape m_selectedShape = null;
	
	private Tooltip m_tooltip = new Tooltip();
	
	@FXML
	Canvas shapeCanvas;
	
	@FXML
	private void initialize() {
		ShapeManager shapeManager = ShapeManager.getShapeManager();
		ObservableList<IShape> shapeList = shapeManager.getShapes();
		shapeList.addListener( new ListChangeListener<IShape>() {
			
			@Override
			public void onChanged( ListChangeListener.Change<? extends IShape> change ) {
	          System.out.println("Detected a change to Observable List! ");
	          while (change.next()) {
	              System.out.println("Was added? " + change.wasAdded());
	              System.out.println("Was removed? " + change.wasRemoved());
	              System.out.println("Was replaced? " + change.wasReplaced());
	              System.out.println("Was permutated? " + change.wasPermutated() );
	          }
	          redraw();
			}
		});
		
		m_tooltip.setAutoHide(true);
		
		installShapeCanvasEventHandlers();		
	}
	
	private void installShapeCanvasEventHandlers() {
		// Install a mouse clicked event handler.
		shapeCanvas.addEventHandler( MouseEvent.ANY, (mouseEvent) -> {
			if ( mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED) {
				System.out.println( "Mouse pressed at <" + mouseEvent.getX() + "," + mouseEvent.getY() + ">");
				
				ShapeManager manager = ShapeManager.getShapeManager();
				ObservableList<IShape> shapes = manager.getShapes();
				for ( IShape shape : shapes ) {
					Boolean bIntersects = shape.intersects(mouseEvent.getX(), mouseEvent.getY());
					
					if ( bIntersects ) {
						System.out.println( "Collision Detected with Shape: " + shape.getName());
						m_selectedShape = shape;
						mouseEvent.consume();
						break;
					}
				}
			}
			else if ( mouseEvent.getEventType() == MouseEvent.MOUSE_DRAGGED) {
				double dXPos = mouseEvent.getX();
				double dYPos = mouseEvent.getY();
				System.out.println( "Mouse dragged at <" + dXPos + "," + dYPos + ">");
				if ( m_selectedShape != null ) {
					// Could get the bounds of the object and use this to determine proper offset for dragging
					// also could add bounds checking here.
					m_selectedShape.setSelected(true);
					m_selectedShape.setXPos( new Double( dXPos).intValue() );
					m_selectedShape.setYPos( new Double( dYPos).intValue() );
					redraw();
					mouseEvent.consume();
				}
			}
			else if ( mouseEvent.getEventType() == MouseEvent.MOUSE_RELEASED) {
				double dXPos = mouseEvent.getX();
				double dYPos = mouseEvent.getY();
				System.out.println( "Mouse released at <" + dXPos + "," + dYPos + ">");
				if ( m_selectedShape != null ) {
					m_selectedShape.setSelected(false);
					// Correct <X,Y> Positions.
					Bounds bounds = m_selectedShape.getBounds();
					// TODO Use previous mouse positions 
					m_selectedShape.setXPos( new Double( dXPos).intValue() );
					m_selectedShape.setYPos( new Double( dYPos).intValue() );
					redraw();
					m_selectedShape = null; // Reset the selected shape.
					mouseEvent.consume();
				}
			}
			else if (mouseEvent.getEventType() == MouseEvent.MOUSE_MOVED) {
				ShapeManager manager = ShapeManager.getShapeManager();
				ObservableList<IShape> shapes = manager.getShapes();
				for ( IShape shape : shapes ) {
					Boolean bIntersects = shape.intersects(mouseEvent.getX(), mouseEvent.getY());
					
					if ( bIntersects ) {
						System.out.println( "Hovering Over: " + shape.getName());

						String msg = new String(
								"Hovering Over Shape: " + shape.getName() +
								"\nLocated at <" + mouseEvent.getX() + "," + mouseEvent.getY() + ">" );	
						m_tooltip.setText( msg );
						if ( !m_tooltip.isShowing()) {
							m_tooltip.show(m_dialogStage);
						}

						mouseEvent.consume();
						break;
					} 
					else {
						System.out.println( "Not Hovering: ");
						m_tooltip.hide();
					}
				}
			}
		});
	}

	
	public void redraw() {
		// First clear the previous canvas visuals.
		clearCanvas();
		
		// Draw all of the shapes contained in the observable list.
		ShapeManager shapeManager = ShapeManager.getShapeManager();
		ObservableList<IShape> shapeList = shapeManager.getShapes();
		GraphicsContext gc = shapeCanvas.getGraphicsContext2D();

		for ( IShape shape : shapeList ) {
			switch( shape.getShapeType()) {
				case Circle:
					drawCircle( gc, shape );
					break;
				case Square:
					drawSquare( gc, shape );
					break;
				case Triangle:
					drawTriangle( gc, shape );
					break;
			}
		}
	}
	
	private void drawCircle( GraphicsContext gc, IShape shape ) {
		setStroke( gc, shape);
		gc.setLineWidth( LINE_WIDTH );
		gc.strokeOval(shape.xPos(), shape.yPos(), shape.size() * IShape.LENGTH, shape.size() * IShape.LENGTH);
	}
	
	private void drawSquare( GraphicsContext gc, IShape shape ) {
		//gc.beginPath();
		setStroke( gc, shape);
		gc.setLineWidth( LINE_WIDTH );
		gc.strokeRect(shape.xPos(), shape.yPos(), shape.size() * IShape.LENGTH, shape.size() * IShape.LENGTH);
		//gc.closePath();
	}
	
	private void drawTriangle( GraphicsContext gc, IShape shape ) {
		setStroke( gc, shape);
		gc.setLineWidth( LINE_WIDTH );
		Integer xOffset = shape.xPos();
		Integer yOffset = shape.yPos();
		Integer lineLenX = shape.size() * IShape.LENGTH;
		Integer lineLenY = shape.size() * IShape.LENGTH;
		gc.strokeLine( lineLenX/2 + xOffset, yOffset, xOffset, lineLenY + yOffset );
		gc.strokeLine( lineLenX/2 + xOffset, yOffset, lineLenX + xOffset, lineLenY + yOffset );
		gc.strokeLine( xOffset, lineLenY + yOffset, lineLenX + xOffset, lineLenY + yOffset );   
	}
	
	private void setStroke( GraphicsContext gc, IShape shape ) {
		if ( shape.isSelected() ) {
			gc.setStroke( Color.AQUA);
		} 
		else {
			gc.setStroke( shape.color() );
		}
	}
	
	public void clearCanvas() {
		GraphicsContext gc = shapeCanvas.getGraphicsContext2D();
		gc.clearRect(0, 0, shapeCanvas.getWidth(), shapeCanvas.getHeight());
	}
	
	public void setDialogStage( Stage dialogStage ) {
		m_dialogStage = dialogStage;
	}
	
	public Boolean isWithinCanvasBounds() {
		Boolean bValid = false;
		// TODO
		return bValid;
	}
}
