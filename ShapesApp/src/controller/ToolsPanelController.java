package controller;

import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.IShape;
import model.ShapeGenerationTask;
import model.ShapeManager;

import java.util.ArrayList;
import java.util.List;

import controller.AddShapeController;
import controller.MainFrameController;

public class ToolsPanelController {
	private Stage m_dialogStage;
	private MainFrameController m_mainController;
	
	@FXML
	Button btAdd, btRemove, btRandom, btClear;
	
	public void setDialogStage( Stage dialogStage ) {
		m_dialogStage = dialogStage;
	}
	
	@FXML
	private void initialize() {

	}
	
	@FXML
	private void onAddButtonClicked() {
		try {
			FXMLLoader addShapeLoader = new FXMLLoader();
			addShapeLoader.setLocation( ToolsPanelController.class.getResource("../view/AddShapeDialog.fxml"));
			AnchorPane addShapeLayout = (AnchorPane ) addShapeLoader.load();
			
			Scene addShapeScene = new Scene( addShapeLayout );
			Stage addShapeDialog = new Stage();
			addShapeDialog.setResizable(false);
			addShapeDialog.setTitle( "Create Shape");
			addShapeDialog.setScene(addShapeScene);
			addShapeDialog.initOwner(m_dialogStage);
			
			AddShapeController addShapeController = addShapeLoader.getController();
			addShapeController.setDialogStage(addShapeDialog);
			
			addShapeDialog.showAndWait();
			
			if ( addShapeController.addButtonClicked() ) {
				System.out.println( "Add Button Clicked");
				IShape shape = addShapeController.getShape();
				assert( shape != null );
				
				// Add the newly created Shape to the Shape Manager's observable list.
				ShapeManager manager = ShapeManager.getShapeManager();
				ObservableList<IShape> shapeList = manager.getShapes();
				shapeList.add(shape);
			}
		}
		catch( Exception e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void onRemoveButtonClicked() {
		TableView<IShape> shapeTable = m_mainController.getShapeOverviewTable();

        int selectedIndex = shapeTable.getSelectionModel().getSelectedIndex();
        if ( selectedIndex >= 0 ) {
        	shapeTable.getItems().remove(selectedIndex);
        }
	}
	
	@FXML
	private void onRandomButtonClicked() {
		onClearButtonClicked();
		System.out.println( "TODO - About to do Random Stuff!");
		createRandomShapes();
	}
	
	private void createRandomShapes() {
		// Create the task.
		ShapeGenerationTask task = new ShapeGenerationTask();
			
		// Construct the progress bar stage.
		ProgressBar bar = new ProgressBar();
		bar.progressProperty().bind(task.progressProperty());
		
		Stage stage = new Stage();
		Group root = new Group();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Progress Controls");
        
        final HBox hb = new HBox();
        hb.setSpacing(5);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().add( bar );
        scene.setRoot(hb);
        stage.show();
        
        // TODO: close the stage when the progress bar is completed

        // Start the thread.
		Thread thread = new Thread(task);
		thread.setDaemon(true );
		thread.start();
		
		task.setOnSucceeded( new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle( WorkerStateEvent e ) {
				System.out.println("Handling Shape Generation Completion");
				stage.close();
				ShapeManager manager = ShapeManager.getShapeManager();
				ObservableList<IShape> shapes = manager.getShapes();
				shapes.addAll( task.getShapes() );
			}
		});
	}
	
	@FXML
	private void onClearButtonClicked() {
		ShapeManager manager = ShapeManager.getShapeManager();
		ObservableList<IShape> shapeList = manager.getShapes();
		shapeList.removeAll(shapeList);
		
		// Note: These values are no longer needed.
		// Calling shapeList.removeAll(shapeList) instead of providing no parameters solved
		// the listener event.
		//m_mainController.clearCanvas();
		//m_mainController.refreshOverviewTable();
		System.out.println( "Removing All Shapes!");
	}
	
	public void setMainController( MainFrameController controller ) {
		m_mainController = controller;
		
        // Disable the remove button if the shape over tab is not visible or the 
		// shape overview table does not have a selected item.
        btRemove.disableProperty().bind( 
        		Bindings.equal( -1, m_mainController.selectedIndexProperty()).or(
				Bindings.not(m_mainController.shapeOverviewTabVisible())));
        
	}
	

}
