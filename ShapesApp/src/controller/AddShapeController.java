package controller;

import java.util.ArrayList;
import java.util.List;


import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.IShape;
import model.ShapeColors;
import model.ShapeFactory;
import model.ShapeTypes;



public class AddShapeController {
	
	// Spinner Constants.
	private static final Integer MAX_SPINNER_LEN = 3;
	private static final String INITIAL_SPINNER_VAL = "0";
	
	// Slider constants.
	private static final Double SLIDER_MIN = 1.0;
	private static final Double SLIDER_MAX = 5.0;
	
	private Stage 	m_dialogStage;
	private IShape  m_shape = null;
	private Boolean m_bAddButtonClicked = false;
	
	@FXML
	TextField efName;
	
	@FXML
	ComboBox<String> cxShape, cxColor;
	
	@FXML
	Slider			slSize;
	
	@FXML
	Spinner<Integer>    spXCoord, spYCoord;
	
	@FXML 
	Button btAdd, btCancel;
	
	@FXML
	Label lbSliderValue;
	
	/*
	 * Constructor
	 */
	public AddShapeController() {
	}

	
	@FXML
	private void initialize() {
		ObjectProperty<SpinnerValueFactory<Integer>> spXIntValFactory = spXCoord.valueFactoryProperty();
		assert( spXIntValFactory != null );
		
		ObjectProperty<SpinnerValueFactory<Integer>> spYIntValFactory = spXCoord.valueFactoryProperty();
		assert( spYIntValFactory != null );
		
		btCancel.setOnAction( this::onCancelButtonClicked );
		
		spXCoord.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				0, 500, Integer.parseInt(INITIAL_SPINNER_VAL) ));
		spXCoord.setEditable(true);

		spYCoord.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				0, 500, Integer.parseInt(INITIAL_SPINNER_VAL) ));
		spYCoord.setEditable(true);
		
		setSpinnerChangeEventListeners();
		
		// Set the slider properties.
		slSize.setMin( SLIDER_MIN);
		slSize.setMax( SLIDER_MAX);
		slSize.setValue( SLIDER_MIN );
		slSize.setShowTickMarks(true);
		slSize.setMajorTickUnit( SLIDER_MIN );
		slSize.setMinorTickCount(0);
		slSize.setBlockIncrement( SLIDER_MIN );
		slSize.setSnapToTicks(true );
		
		// Bind the Slider to the label.
		lbSliderValue.textProperty().bind(
		          Bindings.format(
		              "%.2f",
		              slSize.valueProperty()
		      )
		  );
		
		// Initialize the Shape ComboBox.
		List<String> shapes = ShapeFactory.types();
		
		cxShape.getItems().addAll(shapes);
		cxShape.getSelectionModel().select(0); // Select first item.
		
		// Initialize the Color ComboBox.
		ArrayList<String> colors = new ArrayList<String>();
		colors.add(ShapeColors.Red.toString());
		colors.add(ShapeColors.Blue.toString());
		colors.add(ShapeColors.Green.toString());
		
		cxColor.getItems().addAll(colors);
		cxColor.getSelectionModel().select(0); // Select first item.
	}
	
	
	public void setDialogStage( Stage dialogStage ) {
		m_dialogStage = dialogStage;
	}
	
	private void onCancelButtonClicked( ActionEvent event ) {
		m_dialogStage.close();
	}
	
	private void setSpinnerChangeEventListeners() {
		
		// Monitor the X-Coordinate Spinner.
		spXCoord.getEditor().textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, 
                                            String oldValue, String newValue) {
                if ( (!newValue.matches("\\d+") || newValue.length() > MAX_SPINNER_LEN) 
                		&& newValue.length() != 0 ) {
                	spXCoord.getEditor().setText(oldValue);
                } else if ( newValue.length() > 1 && newValue.charAt(0) == '0') {
                	spXCoord.getEditor().setText(newValue.substring(1)); // Removes leading 0s.
                }
            }
        });
		
		// Monitor the Y-Coordinate Spinner.
		spYCoord.getEditor().textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, 
                                            String oldValue, String newValue) {
                if ( (!newValue.matches("\\d+") || newValue.length() > MAX_SPINNER_LEN) 
                		&& newValue.length() != 0 ) {
                	spYCoord.getEditor().setText(oldValue);
                } else if ( newValue.length() > 1 && newValue.charAt(0) == '0') {
                	spYCoord.getEditor().setText(newValue.substring(1)); // Removes leading 0s.
                }
            }
        });
	}
	
	@FXML
	private void onAddButtonClicked() {
		StringBuilder msg = new StringBuilder();
		if ( validateFields( msg ) ) {
			collectShapeData();
			m_bAddButtonClicked = true;
			
			m_dialogStage.close();
		} else {
			Alert alert = new Alert( AlertType.WARNING);
			alert.setTitle( "Warning Dialog");
			alert.setHeaderText( "Cannot Add Shape!");
			alert.setContentText( msg.toString() );
			
			alert.showAndWait();
		}
	}
	
	private Boolean validateFields( StringBuilder msg) {
		Boolean bValid = true;
		
		// Perform error checking.
		String xCoord = spXCoord.getEditor().getText();
		String yCoord = spYCoord.getEditor().getText();
		if ( efName.textProperty().get().length() == 0 ) {
			msg.append("No Name Provided!\n" );
		} 
		if ( xCoord.length() == 0 ) {
			msg.append( "No X Coord. Provided\n");
			
		} 
		if ( yCoord.length() == 0 ) {
			msg.append( "No Y Coord. Provided\n" );
		}
		
		if ( msg.length() != 0 ) {
			bValid = false;
		}

		return bValid;
	}
	
	private void collectShapeData() {
		// Create the shape type.
		String shapeType = cxShape.getSelectionModel().getSelectedItem();
		m_shape = ShapeFactory.createType( ShapeTypes.valueOf(shapeType));
		
		// Set the name.
		m_shape.setName( efName.textProperty().get() );
		
		// Set the size.
		Double dSliderVal = slSize.getValue();
		m_shape.setSize( dSliderVal.intValue() );
		
		// Set the color.
		String colorString = cxColor.getSelectionModel().getSelectedItem();
		ShapeColors eColor = ShapeColors.valueOf(colorString); 
		
		Color color = ShapeColors.getColor( eColor );		
		m_shape.setColor(color);
		
		String xCoord = spXCoord.getEditor().getText();
		m_shape.setXPos( Integer.parseInt( xCoord ) );
		System.out.println( "X Coord: " + xCoord);
		String yCoord = spYCoord.getEditor().getText();
		m_shape.setYPos(Integer.parseInt( yCoord ) );
		System.out.println( "Y Coord: " + yCoord);
	}
	
	public Boolean addButtonClicked() {
		return m_bAddButtonClicked;
	}
	
	public IShape getShape() {
		return m_shape;
	}
}
