package model;

import javafx.scene.paint.Color;

public enum ShapeColors {
	Red,
	Green,
	Blue;
	
	public static Color getColor( ShapeColors eColor) {
		Color color = null;
		switch ( eColor ) {
			case Red:
				color = Color.RED;
				break;
			case Blue:
				color = Color.BLUE;
				break;
			case Green:
				color = Color.GREEN;
				break;
			default:
				color = Color.RED;
				break;
		}
		return color;
	}
}
