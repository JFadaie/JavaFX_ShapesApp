package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import model.ShapeTypes;

public abstract class IShape {
	
	public static final Integer LENGTH = 25;
	private static final Integer MIN_SIZE = 1;
	private static final Integer MAX_SIZE  = 5;
	
	private final StringProperty 					name;
	private final IntegerProperty					size;
	private final ObjectProperty<Color> 			color;
	private final IntegerProperty					xPosition;
	private final IntegerProperty					yPosition;
	//private final IntegerProperty					zOrder;
	private final ObjectProperty<Bounds> 			bounds;	
	
	private final BooleanProperty					selected;
	
	public static Integer minSize() {
		return MIN_SIZE;
	}
	
	public static Integer maxSize() {
		return MAX_SIZE;
	}
	
	public IShape() {
		this.name = new SimpleStringProperty( null );
		this.size = new SimpleIntegerProperty();
		this.color = new SimpleObjectProperty<Color>(null);
		this.xPosition = new SimpleIntegerProperty();
		this.yPosition = new SimpleIntegerProperty();
		this.bounds = new SimpleObjectProperty<Bounds>(null);
		this.selected = new SimpleBooleanProperty( false );
		
	}
	
	public void initShape( 
			String 		name, 
			Integer 	size, 
			Color 		color, 
			Integer 	xPos, 
			Integer 	yPos ) {
		setName( name );
		setSize( size );
		setColor( color );
		setXPos( xPos);
		setYPos(yPos);
	}
	
	public abstract ShapeTypes getShapeType();
	
	public void setName( String name ) { this.name.set(name); }
	
	public String getName() { return name.get(); }
	
	public void setSize( Integer size ) { this.size.set(size); }
	
	public Integer size() { return size.get(); }
	
	public void setColor( Color color ) { this.color.set(color ); }
	
	public Color color() { return color.get(); }
	
	public void setXPos( Integer xPos ) { this.xPosition.set(xPos); }
	
	public Integer xPos() { return xPosition.get(); }
	
	public void setYPos( Integer yPos ) { this.yPosition.set(yPos); }
	
	public Integer yPos() { return yPosition.get(); }
	
	public Boolean intersects( double x, double y ) {
		Boolean bCollision = false;
		setBoundsProperty();
		Bounds bounds = this.bounds.get();
		bCollision = bounds.contains(x, y);
		return bCollision;
	}	
	
	public Boolean isSelected() {
		return selected.get();
	}
	
	public void setSelected( Boolean bSelected) {
		selected.set(bSelected);
	}
	
	public Bounds getBounds() { 
		setBoundsProperty();
		return bounds.get(); }
	
	// Property Accessors
	public StringProperty getNameProperty() { return name; }
	public StringProperty getSizeProperty() { 
		return new SimpleStringProperty(String.valueOf( size.get() ) ); }
	public StringProperty getColorProperty() { 
		return new SimpleStringProperty(color.get().toString()); }
	public StringProperty getXPosProperty() { 
		return new SimpleStringProperty( String.valueOf( xPosition.get() ) ); }
	public StringProperty getYPosProperty() { 
		return new SimpleStringProperty(String.valueOf( yPosition.get() )); }
	public StringProperty getTypeProperty() { 
		ShapeTypes shapeType = getShapeType();
		return new SimpleStringProperty( shapeType.toString() ); }
	public ReadOnlyObjectProperty<Bounds> getBoundsProperty() { 
		setBoundsProperty();
		return bounds; }
	public BooleanProperty getSelectedProperty() {
		return selected;
	}
	
	private void setBoundsProperty() {
		int iXPos = xPos();
		int iYPos = yPos();
		int iLength = LENGTH * size();
		Bounds bounds = new BoundingBox(iXPos, iYPos, iLength, iLength );
		this.bounds.set( bounds );
	}
}
