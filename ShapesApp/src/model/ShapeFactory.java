package model;

import model.ShapeTypes;
import model.IShape;
import model.Square;
import model.Triangle;

import java.util.ArrayList;
import java.util.List;

import model.Circle;

public class ShapeFactory {

	public static IShape createType( ShapeTypes type ) {
		IShape shape = null;
		switch( type ) {
			case Circle: {
				shape = new Circle();
				break;
			}
			case Triangle: {
				shape = new Triangle();
				break;
			}
			case Square: {
				shape = new Square();
				break;
			}
		}
		
		return shape;
	}
	
	public static List<String> types() {
		ArrayList<String> types = new ArrayList<String>();
		ShapeTypes[] shapeTypes = ShapeTypes.values();
		for ( ShapeTypes shapeType : shapeTypes ) {
			types.add( shapeType.toString() );
		}
		
		return types;
	}
	
}
