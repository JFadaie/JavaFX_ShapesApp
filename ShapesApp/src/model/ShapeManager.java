package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.IShape;

public class ShapeManager {
	private static ShapeManager instance = null;
	
	private ObservableList<IShape> shapeList;
	
	private ShapeManager() {
		shapeList = FXCollections.observableArrayList();
	}
	
	public static ShapeManager getShapeManager() {
		if ( instance == null ) {
			instance = new ShapeManager();
		}
		return instance;
	}
	
	public ObservableList<IShape> getShapes() {
		return shapeList;
	}
}
