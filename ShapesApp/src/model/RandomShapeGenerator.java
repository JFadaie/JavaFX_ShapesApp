package model;

import java.util.Random;

import javafx.scene.paint.Color;
import model.IShape;
import model.ShapeFactory;
import model.ShapeColors;


public class RandomShapeGenerator {
	
	public RandomShapeGenerator() {
		
	}
	
	public static IShape generateShape(
			String name, Integer drawAreaWidth, Integer drawAreaHeight) {
		
		Random rand = new Random();
		ShapeTypes[] shapeTypes = ShapeTypes.values();
		ShapeColors[] shapeColors = ShapeColors.values();
		
		// Create the shape of a random type.
		int shapeTypeIndex = rand.nextInt( shapeTypes.length);
		IShape shape = ShapeFactory.createType(shapeTypes[shapeTypeIndex]);
		
		// Set the name.
		shape.setName(name);
	
		// Specify a random size between MIN and MAX_SIZE.
		Integer size = rand.nextInt( IShape.maxSize()) + IShape.minSize();
		shape.setSize(size);
		
		// Specify a random color between red, green and blue.
		int shapeColorsIndex = rand.nextInt( shapeColors.length);
		Color color = ShapeColors.getColor(shapeColors[shapeColorsIndex]);

		shape.setColor(color);
		
		// Specify a random coordinate position.
		Integer xPos = rand.nextInt( drawAreaWidth);
		Integer yPos = rand.nextInt( drawAreaHeight);
		
		shape.setXPos(xPos);
		shape.setYPos(yPos);
		
		return shape;
	}
}
