package model;

import java.util.ArrayList;
import java.util.List;
import model.IShape;
import model.RandomShapeGenerator;

import javafx.concurrent.Task;

public class ShapeGenerationTask extends Task<Void> {
	private ArrayList<IShape> m_shapes;
	
	private final Integer NUM_SHAPES = 20;
	
	public List<IShape> getShapes() {
		return m_shapes;
	}
	
    @Override 
    public Void call() {
    	m_shapes = new ArrayList<IShape>();
    	
    	updateProgress( 0, NUM_SHAPES);
        int iterations;
        for (iterations = 0; iterations < NUM_SHAPES; iterations++) {
            if (isCancelled()) {
                updateMessage("Cancelled");
                break;
            }
            m_shapes.add( 
            		RandomShapeGenerator.generateShape(Integer.toString(iterations, 10), 500, 500) );
            
            updateMessage("Creating Shape Number " + iterations);
            updateProgress(iterations, NUM_SHAPES);
            
            //Block the thread for a short time, but be sure
            //to check the InterruptedException for cancellation
            try {
                Thread.sleep(100);
            } catch (InterruptedException interrupted) {
                if (isCancelled()) {
                    updateMessage("Cancelled");
                    break;
                }
            }
        }
        
        return null;
    }

}
